<?php

namespace Happen\MyPackage\Dump;

class Dumper
{
    public function __construct($name)
    {
        $this->name = $name;
    }

    function dd($mixed)
    {
        var_dump($mixed . $this->name);
        exit(0);
    }
}
